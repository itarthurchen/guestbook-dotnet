﻿Imports System.Configuration
Imports System.Data.SqlClient

Partial Class Delete
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Session("UserName") <> Nothing And Request.QueryString("id") <> Nothing) Then
            '使用者有登入 而且 有附上想砍的文章ID編號
            Dim SqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("SqlExpress").ConnectionString)
            Dim SqlCommand As New SqlCommand("DELETE FROM [Post] WHERE [ID] = '" + My.Request.QueryString("id") + "' AND [UserName] = '" + Session("UserName") + "'", SqlConnection)
            'Dim SqlCommand As New SqlCommand("DELETE FROM [Post] WHERE [ID] = @ID AND [UserName] = @UserName", SqlConnection)
            'SqlCommand.Parameters.AddWithValue("@ID", Request.QueryString("id"))
            'SqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
            SqlConnection.Open()
            SqlCommand.ExecuteNonQuery()
            SqlConnection.Close()

            Response.Redirect("Default.aspx")
        Else
            Response.Redirect("Default.aspx")
        End If
    End Sub
End Class
