﻿<%@ WebHandler Language="VB" Class="Redirect" %>

Imports System
Imports System.Web

Public Class Redirect : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim TargetUrl As String = context.Request.QueryString.Get("url")
        context.Response.Redirect(TargetUrl)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class