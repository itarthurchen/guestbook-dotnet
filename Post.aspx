﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Post.aspx.vb" Inherits="Post" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Guestbook: Leave a Message</title>
    </head>
    <body>
        <form id="form" runat="server">
            <div style="width: 800px; margin: auto; overflow: hidden;">
            <div style="height: 20px; margin-bottom: 20px;">
                <div style="width: 200px; float: left;">
                    <asp:Literal ID="LiteralUserName" runat="server" />
                    &nbsp;
                </div>
                <div style="width: 400px; float: left;">
                    &nbsp;
                </div>
                <div style="width: 100px; float: left;">
                    &nbsp;
                </div>
                <div style="width: 100px; float: left;">
                    &nbsp;
                </div>
            </div>
            <div>
                <div>
                    <br />
                    Message:
                    <br />
                    <asp:TextBox ID="TextBoxBody" runat="server" TextMode="MultiLine" 
                        Height="150px" Width="448px"></asp:TextBox>
                &nbsp;
                    <asp:Button ID="ButtonPost" runat="server" Text="Post" />
                </div>
            </div>
        </div>
        </form>
    </body>
</html>
