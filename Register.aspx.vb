﻿Imports System.Configuration
Imports System.Data.SqlClient

Partial Class Register
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Request.QueryString.Count > 0) Then
            If (Request.QueryString(0) = "duplicate") Then
                LiteralMessage.Text = "The username is taken. Choose a new one."
            End If
        End If
    End Sub

    Protected Sub ButtonRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonRegister.Click
        If (TextBoxUserName.Text.Length > 0 And TextBoxPassword.Text.Length > 0) Then
            Dim UserName As String = TextBoxUserName.Text.ToLower()
            Dim Password As String = TextBoxPassword.Text

            Using SqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("SqlExpress").ConnectionString)
                Using SqlCommand As New SqlCommand("SELECT [UserName] FROM [User] WHERE [UserName] = '" + UserName + "'", SqlConnection) 'SQL injection
                    'Using SqlCommand As New SqlCommand("SELECT [UserName] FROM [User] WHERE [UserName] = @UserName", SqlConnection) 'SQL injection
                    'SqlCommand.Parameters.AddWithValue("@UserName", UserName)

                    SqlConnection.Open()

                    Using SqlDataReader = SqlCommand.ExecuteReader()
                        If (SqlDataReader.Read) Then
                            SqlConnection.Close()
                            Response.Redirect("register.aspx?duplicate")
                        Else
                            SqlConnection.Close()
                        End If
                    End Using
                End Using

                Using SqlCommand As New SqlCommand("INSERT INTO [User] ([UserName], [Password]) VALUES ('" + UserName + "', '" + Password + "')", SqlConnection) 'SQL injection
                    'Using SqlCommand As New SqlCommand("INSERT INTO [User] ([UserName], [Password]) VALUES (@UserName, @Password)", SqlConnection) 'SQL injection
                    'SqlCommand.Parameters.AddWithValue("@UserName", UserName)
                    'SqlCommand.Parameters.AddWithValue("@Password", Password)

                    SqlConnection.Open()
                    SqlCommand.ExecuteNonQuery()
                    SqlConnection.Close()
                End Using
            End Using

            Session("UserName") = UserName

            Response.Redirect("Default.aspx")
        End If
    End Sub
End Class