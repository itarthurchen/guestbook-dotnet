Imports System.Configuration
Imports System.Data.SqlClient

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim random = New Random
        random.Next()
        ' Check if user is logged in
        If (Session("UserName") <> Nothing) Then
            LiteralUserName.Text = "Hi " + Session("UserName").ToString()
            HyperLinkLogin.Text = "Logout"
            HyperLinkLogin.NavigateUrl = "logout.aspx"

            HyperLinkPost.Text = "Leave a Message"
            HyperLinkPost.NavigateUrl = "post.aspx"
        Else
            HyperLinkLogin.Text = "Login"
            HyperLinkLogin.NavigateUrl = "login.aspx"

            HyperLinkPost.Text = "Register"
            HyperLinkPost.NavigateUrl = "register.aspx"
        End If

        Dim Page As Int32 = 1
        If (Request.QueryString("p") <> Nothing) Then
            Try
                Page = Convert.ToInt32(Request.QueryString("p"))
            Catch ex As Exception

            End Try
        End If

        Dim SqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("SqlExpress").ConnectionString)
        Dim SqlCommand As New SqlCommand("SELECT COUNT([ID]) FROM [Post]", SqlConnection)
        SqlConnection.Open()
        Dim SqlDataReader = SqlCommand.ExecuteReader()
        If (SqlDataReader.Read) Then
            For Counter As Int32 = 1 To Math.Floor(SqlDataReader.GetInt32(0) / 10) + 1 Step 1
                LiteralPages.Text += "<a href=""/default.aspx?p=" + Counter.ToString() + """>" + Counter.ToString() + "</a> "
            Next
        End If
        SqlConnection.Close()

        ' DataFlow Analysis
        Dim query = Request.QueryString("q")
        Response.Write(query)
        ' DataFlow Analysis
        Dim cmd As New SqlCommand("SELECT * From [Post] Where [UserName] = '" + query + "'")

        If (Request.QueryString("query") <> Nothing) Then
            Dim s As String
            s = Request.QueryString("query").ToString()
            SqlCommand.CommandText = "SELECT TOP(10) [ID], [UserName], [Body], [DateTime] FROM [Post] WHERE [UserName] = '" + s + "' And [ID] NOT IN (SELECT TOP((" + Page.ToString + " - 1) * 10) [ID] FROM [Post] ORDER BY [DateTime] DESC) ORDER BY [DateTime] DESC"
        Else
            SqlCommand.CommandText = "SELECT TOP(10) [ID], [UserName], [Body], [DateTime] FROM [Post] WHERE [ID] NOT IN (SELECT TOP((" + Page.ToString + " - 1) * 10) [ID] FROM [Post] ORDER BY [DateTime] DESC) ORDER BY [DateTime] DESC"
        End If

        'SqlCommand.CommandText = "SELECT TOP(10) [ID], [UserName], [Body], [DateTime] FROM [Post] WHERE [ID] NOT IN (SELECT TOP((@Page - 1) * 10) [ID] FROM [Post] ORDER BY [DateTime] DESC) ORDER BY [DateTime] DESC"
        'SqlCommand.Parameters.AddWithValue("@Page", Int32Page)

        SqlConnection.Open()
        SqlDataReader = SqlCommand.ExecuteReader()

        While (SqlDataReader.Read())
            Dim UserName = SqlDataReader.GetString(1)
            Dim Body = SqlDataReader.GetString(2)

            LiteralPosts.Text += "<div style=""margin-bottom: 10px;"">"
            LiteralPosts.Text += "<div>" + UserName + " posted "
            LiteralPosts.Text += "at " + SqlDataReader.GetDateTime(3).ToString() + "</div>"
            LiteralPosts.Text += "<div style=""margin-left: 30px;"">" + Body + "</div>"

            If (Session("UserName") <> Nothing) Then
                If (Session("UserName").ToString() = UserName.ToLower()) Then
                    LiteralPosts.Text += "<div><a href=""/delete.aspx?id=" + SqlDataReader.GetInt32(0).ToString() + """>Delete it</a></div>"
                End If
            End If

            LiteralPosts.Text += "</div>"
        End While

        SqlConnection.Close()

        ' DataFlow Analysis
        Try
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try

        ' DataFlow Analysis
        Dim url = Request.QueryString("q")
        Response.Redirect(url)

    End Sub
End Class
