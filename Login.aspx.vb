﻿Imports System.Configuration
Imports System.Data.SqlClient

Partial Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Request.QueryString.Count > 0) Then
            If (Request.QueryString(0) = "incorrect") Then
                LiteralMessage.Text = "The username/password combination is incorrect."
            End If
        End If
    End Sub

    Protected Sub ButtonLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonLogin.Click
        If (TextBoxUserName.Text.Length > 0 And TextBoxPassword.Text.Length > 0) Then
            Dim UserName As String = TextBoxUserName.Text.ToLower()
            Dim Password As String = TextBoxPassword.Text

            Dim SqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("SqlExpress").ConnectionString)
            Dim SqlCommand As New SqlCommand("SELECT [UserName] FROM [User] WHERE [UserName] = '" + UserName + "' AND [Password] = '" + Password + "'", SqlConnection) 'SQL injection, UserName and Password are user input and should not be trusted_
            'Dim SqlCommand As New SqlCommand("SELECT [UserName] FROM [User] WHERE [UserName] = @UserName AND [Password] = @Password", SqlConnection)
            'SqlCommand.Parameters.AddWithValue("@UserName", UserName)
            'SqlCommand.Parameters.AddWithValue("@Password", Password)

            SqlConnection.Open()
            Dim reader = SqlCommand.ExecuteReader()
            If (reader.Read) Then
                Session("UserName") = reader.GetString(0)

                ' DataFlow Analysis
                Dim lala As String = reader.GetString(0)
                LabelLala.Text = lala

                ' DataFlow Analysis
                Dim kiki As String = LabelLala.Text
                LabelLala.Text = kiki

                SqlConnection.Close()
                Response.Redirect("Default.aspx")
            Else
                SqlConnection.Close()
                Response.Redirect("login.aspx?incorrect")
            End If
        End If
    End Sub
End Class
