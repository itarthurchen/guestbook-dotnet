﻿Imports System.Configuration
Imports System.Data.SqlClient

Partial Class Post
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Check if user is logged in
        If (Session("UserName") <> Nothing) Then
            LiteralUserName.Text = "Hi, " + Session("UserName").ToString()
        Else
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Protected Sub ButtonPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonPost.Click
        Dim SqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("SqlExpress").ConnectionString)
        Dim SqlCommand As New SqlCommand("INSERT INTO [Post] ([UserName], [Body], [DateTime]) VALUES ('" + Session("UserName").ToString() + "', N'" + Replace(TextBoxBody.Text, System.Environment.NewLine, "<br />") + "', '" + DateTime.Now.ToString() + "')", SqlConnection) 'SQL injection
        'Dim SqlCommand As New SqlCommand("INSERT INTO [Post] ([UserName], [Body], [DateTime]) VALUES (@UserName, @Body, @DateTime)", SqlConnection)
        'SqlCommand.Parameters.AddWithValue("@UserName", Session("UserName"))
        'SqlCommand.Parameters.AddWithValue("@Body", TextBoxBody.Text.Replace(System.Environment.NewLine, "<br />"))
        'SqlCommand.Parameters.AddWithValue("@DateTime", DateTime.Now)
        SqlConnection.Open()
        SqlCommand.ExecuteNonQuery()
        SqlConnection.Close()

        Response.Redirect("Default.aspx")
    End Sub
End Class
