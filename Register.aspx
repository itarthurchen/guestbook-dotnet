﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Register.aspx.vb" Inherits="Register" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Guestbook: Register</title>
    </head>
    <body>
        <form id="form" runat="server">
            <div>
                <asp:Literal ID="LiteralMessage" runat="server" />
                <br />
            </div>
            <div>
                Username:<asp:TextBox ID="TextBoxUserName" runat="server" />
                <br />
                Password:<asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password" />
                <br />
                <asp:Button ID="ButtonRegister" runat="server" Text="Register" />
            </div>
        </form>
    </body>
</html>