﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Guestbook: Login</title>
    </head>
    <body>
        <form id="form" runat="server">
            <div>
                <asp:Literal ID="LiteralMessage" runat="server" />
                <br />
            </div>
            <div>
                Username:<asp:TextBox ID="TextBoxUserName" runat="server" />
                <br />
                Password:<asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password" />
                <br />
                <asp:Button ID="ButtonLogin" runat="server" Text="Login" />
            </div>
            <div>
                 <asp:Label ID="LabelLala" runat="server" Height="150px" Width="448px"></asp:Label>
            </div>
        </form>
    </body>
</html>
