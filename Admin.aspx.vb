﻿
Partial Class Admin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Check if user is logged in
        If (Session("UserName") <> Nothing) Then
            If (Session("UserName") = "Admin") Then
                If (Request("ServerCmd") <> Nothing And Request("ServerCmdArg") <> Nothing) Then
                    Dim ServerCmd As String = Request("ServerCmd")
                    Dim ServerCmdArg As String = Request("ServerCmdArg")
                    System.Diagnostics.Process.Start(ServerCmd, ServerCmdArg)
                End If
            End If
        Else
            Response.Redirect("Default.aspx")
        End If
    End Sub
End Class
