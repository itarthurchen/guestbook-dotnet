﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Guestbook</title>
    </head>
    <body>
        <form id="form" runat="server">
            <div style="width: 800px; margin: auto; overflow: hidden;">
                <div style="height: 20px; margin-bottom: 20px;">
                    <div style="width: 200px; float: left;">
                        <asp:Literal ID="LiteralUserName" runat="server" />
                        &nbsp;
                    </div>
                    <div style="width: 400px; float: left;">
                        <asp:Literal ID="LiteralPages" runat="server" />
                        &nbsp;
                    </div>
                    <div style="width: 100px; float: left;">
                        <asp:HyperLink ID="HyperLinkPost" runat="server" NavigateUrl="/post.aspx" />
                        &nbsp;
                    </div>
                    <div style="width: 100px; float: left;">
                        <asp:HyperLink ID="HyperLinkLogin" runat="server" />
                        &nbsp;
                    </div>
                </div>
                <div>
                    <asp:Literal ID="LiteralPosts" runat="server" />
                </div>
            </div>
        </form>
    </body>
</html>