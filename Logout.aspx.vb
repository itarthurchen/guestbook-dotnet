﻿
Partial Class Logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("UserName") = Nothing

        Response.Redirect("Default.aspx")
    End Sub
End Class
